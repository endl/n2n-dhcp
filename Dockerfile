FROM debian:stretch

COPY ./sources.list /etc/apt/sources.list

COPY n2n.deb /usr/bin
WORKDIR /usr/bin

RUN apt-get update \
 && dpkg -i n2n.deb

#COPY --from=builder /usr/src/n2n/supernode /usr/bin
#COPY --from=builder /usr/src/n2n/edge /usr/bin

EXPOSE 7654 7654/udp
EXPOSE 5645 5645/udp

#ENTRYPOINT ["/usr/bin/supernode", "-f"]